
# TODO:

[x] -> completed. 
 
[ ] -> not completed.

Changes made after user testing during tutorial on Monday, November 14th.
```
- [x] Sidebar       => Add pointer
- [x] Table         => Numbers are sortable
- [x] Table         => Clicking text brings back original sort 
- [x] Table         => Add sort notification
- [x] Table         => Add sort help section 
- [x] Table         => Rows can be selected 
- [x] Home          => Update Today 
- [x] Home          => Add Weather 
- [ ] Leaderboard   => Add you (bolded). Table would need to be refactored. prolly no.
- [x] Leaderboard   => Indicator for what this page does 
- [ ] Bills         => Indicator for what this page does 
- [x] Bills         => Clickable rows for table 
- [x] Bills         => Count for clicked rows (month) 
- [x] Bills         => Button to view comparison of selected rows
- [x] Bills         => Compare monthly bills 
- [x] Bills         => Comparing bills shows their pictures
- [x] Bills         => Select electricity or water bills. 
- [x] Bills         => Selecting 0 bills does nothing.
- [x] Bills         => Text changes for 0 selected, 1 selected,
                       and more than 1 selected
- [x] Bills         => Pay bill section
- [ ] Bills         => Range
- [x] Usage         => Indicator for what this page does 
- [x] Usage         => Graph comparing hourly rates 
- [x] Usage         => `Add appliance` (comping soon^TM) 
- [x] Usage         => Select month, week, year. 
- [x] Tips          => Indicator for what this page does 
- [x] Tips          => Counter for completed `Tips` 
- [x] Tips          => Clicking `Tips` makes them green 
- [x] Tips          => Tips do not get removed on click 
- [x] Help          => Add Help Page 
- [x] Help          => Add Help text 
- [x] About         => Add About Page 
- [x] About         => Add copyright stuff (most under MIT license) 
- [x] Design        => Consistent look, colors
```
